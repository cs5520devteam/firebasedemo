package edu.neu.madcourse.lecture6.firebasedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FakeCallActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fake_call);
    }
}
